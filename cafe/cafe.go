package cafe

import (
	"fmt"
	drawing "gitlab.com/eper.io/engine/drawing"
	"gitlab.com/eper.io/engine/englang"
	"gitlab.com/eper.io/engine/metadata"
	"net/http"
	"strings"
	"time"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

// This is a plain message board application
// You can use it for public accounts for semi-private communication.
// The server side stores the messages it is not end-to-end encrypted.

// Advantages
//
// Simple to use and to sign up. It is ideal for busy public places like airports.
// There is no account. Signing up is hassle-free.
// We use a unique browser url api key per user.
// Do you want to share? Send the browser url.
// Do you want to return? Save the browser url.
// It is declared non-encrypted. We still use TLS, but just for the browser.
// You watch what you talk about as if you were talking in a public restaurant.
// This mindset calms your mind, and it causes happiness.
// There are no cookies kept in the browser eliminating any privacy issues.
// Messages are deleted within a period of time (i.e. defaultRetention)
// Open source code base with the most favorable Creative Commons Zero license.

// The service is still free, but you can donate us, if you like it.
// Do you have Business? www.schmied.us
// The technology is suitable for vending machines, cloud service, industrial devices, demonstrations.

const defaultRetention = 168 * time.Hour

var defaultText = fmt.Sprintf(drawing.RevertAndReturn+
	`Leave a message.

Share the unique browser url with peers.

This is a public channel like a street cafe.

It is less public and more relaxing than social media.

No browser cookies stored.

No content is kept after %s hours.

Click the right corner to clear the note.

    HAVE FUN! - The authors
`, englang.DecimalString(int64(defaultRetention.Hours()+0.99)))

func SetupCafe() {
	http.HandleFunc("/index.html", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, fmt.Sprintf("/cafe.html"), http.StatusTemporaryRedirect)
	})
	http.HandleFunc("/icon.png", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-cache")
		http.ServeFile(w, r, "./cafe/res/eper.png")
	})

	http.HandleFunc("/cafe.html", func(w http.ResponseWriter, r *http.Request) {
		err := drawing.EnsureAPIKey(w, r)
		if err != nil {
			return
		}

		if doWeRedirectToAnExistingSession(w, r) {
			return
		}

		drawing.ServeRemoteForm(w, r, "cafe")
	})

	http.HandleFunc("/cafe.png", func(w http.ResponseWriter, r *http.Request) {
		drawing.ServeRemoteFrame(w, r, declareCafeForm)
	})

	go func() {
		for {
			cafeMessageRetentionAndGarbageCollection()
			time.Sleep(25 * time.Minute)
		}
	}()
}

func cafeMessageRetentionAndGarbageCollection() {
	for k, v := range conversations {
		var checksum int32
		for _, ch := range v {
			checksum = checksum + ch
		}
		x := englang.Printf("Conversation %s with checksum %s cleans up on %s .", k, englang.DecimalString(int64(checksum)), time.Now().Add(defaultRetention).Format("2006-01-02"))
		last, ok := garbage[k]
		if ok {
			var key, base, deadline string
			err := englang.Scanf1(last, "Conversation %s with checksum %s cleans up on %s .", &key, &base, &deadline)
			if err == nil {
				deadlineS, err := time.Parse("2006-01-02", deadline)
				if err == nil {
					if deadlineS.Before(time.Now()) {
						fmt.Println("Deleting, ", x)
						delete(conversations, key)
						for participantKey, v := range participants {
							if v == key {
								delete(participants, participantKey)
							}
						}
					}
				}
			}
		} else {
			garbage[k] = x
		}
	}
}

func doWeRedirectToAnExistingSession(w http.ResponseWriter, r *http.Request) bool {
	apiKey := r.URL.Query().Get("apikey")
	conversation, ok := participants[apiKey]
	if !ok {
		newConversation := drawing.GenerateUniqueKey()
		participants[apiKey] = newConversation
		conversations[newConversation] = defaultText
	} else {
		var conversationId string
		if englang.Scanf1(conversation, "Create new session pointing to conversation %s.", &conversationId) != nil {
			// New UI session for the existing conversation
			newParticipant := drawing.GenerateUniqueKey()
			participants[newParticipant] = englang.Printf("Create new session pointing to conversation %s.", conversation)
			w.Header().Set("Location", r.URL.EscapedPath()+fmt.Sprintf("?apikey=%s", newParticipant))
			w.WriteHeader(http.StatusTemporaryRedirect)
			return true
		} else {
			participants[apiKey] = conversationId
		}
	}
	return false
}

func declareCafeForm(session *drawing.Session) {
	if session.Form.Boxes == nil {
		drawing.DeclareForm(session, "./cafe/res/cafe.png")

		const Donate = 0
		const Message = 1
		const Page = 2

		init := conversations[participants[session.ApiKey]]
		drawing.PutText(session, Message, drawing.Content{Text: init, Lines: 16, Editable: true, Selectable: false, FontColor: drawing.Black, BackgroundColor: drawing.Transparent, Alignment: 1})
		drawing.PutText(session, Page, drawing.Content{Text: "", Lines: 1, Editable: false, Selectable: false, FontColor: drawing.Black, BackgroundColor: drawing.White, Alignment: 1})
		session.SelectedBox = Message

		session.SignalRecalculate = func(session *drawing.Session) {
			text := conversations[participants[session.ApiKey]]
			drawing.PutText(session, Message, drawing.Content{Text: text, Lines: 16, Editable: true, FontColor: drawing.Black, BackgroundColor: drawing.Transparent, Alignment: 1})
			session.SignalPartialRedrawNeeded(session, Message)
		}
		session.SignalTextChange = func(session *drawing.Session, i int, from string, to string) {
			propagateChanges(session, i, from, to)
		}
		session.SignalClicked = func(session *drawing.Session, i int) {
			if i == Message {
				session.Data = fmt.Sprintf("/typing.html?apikey=%s", session.ApiKey)
				session.SignalClosed(session)
				return
			}
			if i == Page {
				content := session.Text[Message]
				content.Text = "\rNew page started"
				session.Text[Message] = content
				conversation := participants[session.ApiKey]
				conversations[conversation] = content.Text
				session.SignalFullRedrawNeeded(session)
				return
			}
			if i == Donate {
				session.Data = metadata.PaymentUrl
				session.SignalClosed(session)
			}
		}
		session.SignalClosed = func(session *drawing.Session) {
			session.SelectedBox = -1
			session.Redirect = session.Data
		}
		session.SignalFocusChanged = func(session *drawing.Session, i int, i2 int) {
			session.SignalPartialRedrawNeeded(session, Page)
			session.SignalPartialRedrawNeeded(session, Message)
		}
		session.SignalFullRedrawNeeded(session)
	}
}

func propagateChanges(session *drawing.Session, i int, from string, to string) {
	conversation := participants[session.ApiKey]
	committed := conversations[conversation]
	if committed == from || strings.HasPrefix(committed, "\r") || strings.HasPrefix(from, "\r") {
		conversations[conversation] = to
		if i != -1 {
			session.SignalPartialRedrawNeeded(session, i)
		}
		go func() {
			sample := participants
			for k, v := range sample {
				if v == conversation && session.ApiKey != k {
					drawing.RecalculateSession(k)
				}
			}
		}()
	}
}
