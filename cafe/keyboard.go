package cafe

import (
	"fmt"
	drawing "gitlab.com/eper.io/engine/drawing"
	"net/http"
	"strings"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

// This is a module that implements a ghost keyboard.
// We call it ghost instead of virtual because virtual has some negative meaning to date.
// You type English here. We can add other languages later.
// Double press returns large caps, triple press allows typing double small caps like bamboo.
// Special characters are not hidden behind a control button and another keyboard.
// You rather type them out. You use them rarely typing is easier than trying to find it elsewhere.
// 'How do you get that symbol keyboard with the copyright sign???' 'Just use creative commons...'

var speakMap = map[string]string{
	"hash�":        "#",
	"exclamation�": "!",
	"exc�":         "!",
	"at�":          "@",
	"dollar�":      "$",
	"dol�":         "$",
	"percent�":     "%",
	"per�":         "%",
	"power�":       "^",
	"pow�":         "^",
	"and�":         "&",
	"star�":        "*",
	"str�":         "*",
	"open�":        "(",
	"ope�":         "(",
	"close�":       ")",
	"clo�":         ")",
	"begin�":       "{",
	"beg�":         "{",
	"end�":         "}",
	"start�":       "[",
	"sta�":         "[",
	"finish�":      "]",
	"fin�":         "]",
	"s.�":          "ẞ",
	"a.�":          "ä",
	//"ä�":           "Ä",
	"\n�": "#",
}

func SetupTyping() {
	http.HandleFunc("/typing.html", func(w http.ResponseWriter, r *http.Request) {
		err := drawing.EnsureAPIKey(w, r)
		if err != nil {
			return
		}

		if doWeRedirectToAnExistingSession(w, r) {
			return
		}

		drawing.ServeRemoteForm(w, r, "typing")
	})

	http.HandleFunc("/typing.png", func(w http.ResponseWriter, r *http.Request) {
		drawing.ServeRemoteFrame(w, r, declareKeyboardForm)
	})

}

func declareKeyboardForm(session *drawing.Session) {
	if session.Form.Boxes == nil {
		drawing.DeclareForm(session, "./cafe/res/typing.png")

		const Donate = 0
		const Message = 1
		const Keyboard = 2
		const keys = "1234567890qwertyuiopasdfghjkl\n#zxcvbnm,. "

		session.SelectedBox = -1
		const DefaultInstructions = "\rClick here for backspace. aaa writes A ...\n at# writes @ ... hash# writes # Use single # when done ..."
		drawing.PutText(session, Donate, drawing.Content{Text: "", Lines: 0, Editable: false, Selectable: false, FontColor: drawing.Black, BackgroundColor: drawing.Transparent, Alignment: 1})
		drawing.PutText(session, Message, drawing.Content{Text: DefaultInstructions, Lines: 2, Editable: true, Selectable: false, FontColor: drawing.Black, BackgroundColor: drawing.Transparent, Alignment: 1})

		for i, _ := range keys {
			drawing.PutText(session, Keyboard+i, drawing.Content{Text: "", Lines: 1, Editable: false, Selectable: false, FontColor: drawing.Black, BackgroundColor: drawing.White, Alignment: 1})
		}
		session.SelectedBox = Message

		session.SignalRecalculate = func(session *drawing.Session) {
		}
		session.SignalClicked = func(session *drawing.Session, i int) {
			if i != -1 {
				replace := session.Text[Message]
				newMessage := replace.Text
				if strings.HasPrefix(replace.Text, "\r") {
					replace.Text = "�"
				}
				if i == Message {
					b, e, ok := strings.Cut(newMessage, "�")
					if ok && len(b) >= 1 {
						chars := []rune(b)
						newMessage = ""
						for i, c := range chars {
							if i < len(chars)-1 {
								newMessage = newMessage + string(c)
							}
						}
						newMessage = newMessage + "�" + e
					}
					replace.Text = newMessage
					session.Text[Message] = replace
					session.SignalPartialRedrawNeeded(session, Message)
					return
				} else {
					addedChar := keys[i-Keyboard]
					added := string(addedChar)
					addedCaps := ""
					if addedChar >= 'a' && addedChar <= 'z' {
						addedCaps = string(addedChar - 'a' + 'A')
					}

					if added == "#" {
						for k, v := range speakMap {
							if strings.HasSuffix(replace.Text, k) {
								added = v
								replace.Text = strings.Replace(replace.Text, k, "�", 1)
								break
							}
						}
					}
					if added == "#" {
						doneTyping(session, i, replace.Text)
						return
					}
					if strings.Contains(replace.Text, added+added+"�") {
						// Double click is a to A
						if len(added) == 1 && added[0] >= 'a' && added[0] <= 'z' {
							replace.Text = strings.Replace(replace.Text, added+added+"�", "�", 1)
							added = string('A' + (added[0] - 'a'))
						}
					} else if strings.Contains(replace.Text, addedCaps+"�") {
						// Triple click is a to A, then aa.
						if len(added) == 1 && added[0] >= 'a' && added[0] <= 'z' {
							replace.Text = strings.Replace(replace.Text, addedCaps+"�", "�", 1)
							added = string(addedChar) + string(addedChar)
						}
					}
					newMessage = strings.ReplaceAll(replace.Text, "�", added+"�")
					if strings.Contains(replace.Text, "\n\n�") {
						replace.Text = strings.Replace(replace.Text, "\n\n�", "�", 1)
						doneTyping(session, -1, replace.Text)
					}
				}
				session.SignalTextChange(session, Message, replace.Text, newMessage)
				replace.Text = newMessage
				session.Text[Message] = replace
				session.SignalPartialRedrawNeeded(session, Message)
			}
		}
		session.SignalFullRedrawNeeded(session)
	}
}

func doneTyping(session *drawing.Session, i int, replace string) {
	propagateChanges(session, -1, "\r", replace)
	session.Redirect = fmt.Sprintf("/cafe.html?apikey=%s", session.ApiKey)
	session.SignalClosed(session)
	return
}
