# Public Cafe Chat App

Public Cafe is a non-encrypted chat program.

It is branded publicly as Open Air Cafe. It promotes open communication.
It is a simple psychological experiment that brings you back to a society where communication relies less on chat rooms, crypto, certificates, etc.

It allows you to run applets to chat with family and random people about general things over the internet.

## Design

The communication is public. You cannot anticipate anything, you need to make sure who is on the other side.

There are some limitations of the fonts that need to be improved later.

Do you have business? Contact hq@schmied.us

## Who is it for?

It is for people who want to try how was it to communicate over the public internet.

It is not enclosed into social media rules, private rooms, secret corners.

It just gives you a feeling.

## Advantages

- Simple to use and to sign up. It is ideal for busy public places like airports.
- There is no account. Signing up is hassle-free.
- We use a unique api key per user in the browser url.
- Do you want to share? Send the browser url.
- Do you want to return? Save the browser url.
- It is declared to be non-encrypted. We still use TLS, but just for the browser.
- You watch what you talk about as if you were talking in a public restaurant.
- This mindset calms your mind, and it causes happiness.
- There are no cookies kept in the browser eliminating any privacy issues.
- Messages are deleted within a period of time (i.e. defaultRetention)
- Open source code base with the most favorable Creative Commons Zero license.

## Getting started

```
git clone https://gitlab.com/eper.io/publiccafe.git
```

## License

```
This document is Licensed under Creative Commons CC0.
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this document to the public domain worldwide.
This document is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this document.
If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.
```
